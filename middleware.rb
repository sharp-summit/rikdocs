require 'sinatra/base'

# 为保证此处视图渲染位置与app.rb路径中一致 须放在最外层 否则需要在代码中处理views路径问题
class LoginScreen < Sinatra::Base
  enable :sessions

  get('/login') { erb :login }

  post('/login') do
    if params['name'] == 'admin' && params['password'] == '123qwe'
      session['user_name'] = params['name']
      redirect '/'
    else
      redirect '/login'
    end
  end
end