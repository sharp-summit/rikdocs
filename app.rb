Encoding.default_external = Encoding.find('utf-8')

require 'sinatra'
require 'rdiscount'
#require "sinatra/reloader" if development?
require 'date'
#require './middleware'
$LOAD_PATH << File.dirname(__FILE__)+"/lib"
require 'doc'

  # class SassHandler < Sinatra::Base
  #   set :views, File.dirname(__FILE__) + '/assets/stylesheets'
    
  #   get '/css/*.css' do
  #       filename = params[:splat].first
  #       scss filename.to_sym
  #   end
  # end
#class RikDocsApp < Sinatra::Base
# use SassHandler
  configure do
    register Sinatra::Compass
    set :title=>'Rik-Docs'
    #set :markdown,:layout_engine=>:erb #,:layout_options => { :views => 'views/layouts' } 
    # set :views=> settings.root+"/views"
    set :erb,:layout=>:"layouts/layout"
    set :public_folder => File.dirname(__FILE__)+'/assets'

    Doc.load("#{settings.views}/documents")
  end


  helpers do
    def docs   # 文档集合
      Doc.doclist
    end

    def archives  # 文档按年份分组集合
      docs.group_by { |a| a[:date].split('-')[0] }
    end

    def parseDate(str,fmt='')   #string转换日期 fmt such:"%Y/%m/%d"
      date=Date.parse(str)      # if invalid throw ex.
      return date.strftime(fmt) if !fmt.empty?  # is format string
      return date               # is Date object,such: date.year...
    end

    def link(str,ext=".html") # 封装url辅助方法,增加后缀配置
      "#{url(str)}#{ext}"
    end
  end

  get '/' do
    erb :index
  end

  get '/login' do
    erb :login
  end

  #get '/:year/:month/:day/:slug.*' do |y,m,d,s,ext|
  #get '/:year/:month/:day/:slug.:ext' do |y,m,d,s,ext|
  get '/:year/:month/:day/:slug.html' do |y,m,d,s|
    doc=Doc["#{y}-#{m}-#{d}-#{s}"]
    # if doc.nil?
    #   halt 400
    # end
    pass unless !doc.nil?

    erb :"layouts/page",:locals=>doc.to_hash
  end

  get '/archives/?' do
    erb :archives
  end

  get '/about/?' do
    erb :about
  end
  # get '/test' do
  #   erb :page do
  #     erb :test
  #   end
  #   #erb :intro,:layout=>:page
  # end

  get '/stylesheets/:name.css' do
    scss :"stylesheets/#{params["name"]}"
  end

#end 