Encoding.default_external = Encoding.find('utf-8')
require 'yaml'
require 'rdiscount'

class Doc

  class << self

    def load(dir,ext='md') 
      @doclist ||= {}
      @dirname=dir
      Dir.glob("#{dir}/**/*.#{ext}").each do |file|
        document = Doc.new(file)
        @doclist[document.slug] = document
        #@doclist << document.slug.to_sym=>document
      end
    end

    def [](slug)
      @doclist[slug]
    end

    def doclist
      @doclist.values.sort.reverse
    end
  end

  def initialize(file_path)
    @file_path = file_path
    #yaml = File.new(file_path).lines.take_while{ |line| !line.strip.empty? }
    file,yaml,lineno=File.new(file_path),'',0
    file.each { |line|
      if line.to_s.strip.empty?
        lineno=file.lineno
        break;
      end
      yaml << line
    }
    @offset = lineno
    @options = YAML.load(yaml)
    # @doclist=[]
    # recursion(path,@navlist)
  end

  %w(title date author summary).each do |m|
    class_eval "def #{m};@options['#{m}'];end"
  end

  def slug
    File.basename(@file_path, ".*")
  end

  def <=>(doc)
    self.date <=> doc.date
  end

  def [](key)
    @options[key.to_s]
  end

  def body
    RDiscount.new(content).to_html
  end

  def summary
    if @options.has_key?("description")
      @options["description"].to_s
    else
      RDiscount.new(content[0..100]).to_html
    end
  end

  def path # 如果生成地址不正确先检查文件名中的日期与yaml配置日期是否一致
    date = @options['date']
    return '' if date.empty?
    date=Date.parse(date)
    if slug.start_with? date.strftime("%F")
      slug.split("-",4).join("/")
    else
      "/#{date.strftime("%Y/%m/%d")}/#{slug}"
    end
  end

  def to_hash
    Hash[%w(title date author summary body).map{ |key| [key.intern, self.send(key)] }]
  end

  private
  def content
    return @content if instance_variable_defined?("@content")
    File.new(@file_path).readlines.drop(@offset).join
  end

  def recursion(path,list)
    if File.directory?(path)
      dir=Dir.open(path)
      while name=dir.read
        next if name=="."
        next if name==".."
        next if name.include?(".") && !name.end_with?(".md")
        nav=Nav.new
        nav.name=name
        _child=[]
        recursion(path+"/"+name,_child)
        nav.children=_child
        if name.end_with?(".md")
          #tmp=File.new(path+"/"+name).readlines # 注意原先用的.lines是each_line已过时的别名 读取所有行到数组里可以用这个
          #yaml = tmp.take_while{ |line| !line.strip.empty? }
          file,_yaml,_yamlLength=tmp=File.new(path+"/"+name),"",0
          file.each { |f| 
            if f.to_s.strip.empty?
              _yamlLength=file.lineno
              break;
            end
            _yaml << f
          }
          nav.name=YAML.load(_yaml)["title"]
          #nav.name=YAML.load(yaml.join)["title"]
        end
        #nav.hasChildren=true
        list.push(nav)
      end
      dir.close
    end
  end

end

class Nav
  def name
    @name
  end

  def name=(value)
    @name=value
  end

  def children
    @children
  end

  def children=(value)
    @children=value
    @hasChildren=(!value.nil?) && value.length>0
  end

  def hasChildren
    @hasChildren
  end

  def hasChildren=(value)
    @hasChildren=value
  end

  def to_json(*a)
    {
      'name'=>@name,
      'items'=>@children,
      'hasChildren'=>@hasChildren
    }.to_json(*a)
  end

end